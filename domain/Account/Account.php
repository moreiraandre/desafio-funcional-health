<?php

namespace Domain\Account;

use App\Http\Resources\Account\Balance as BalanceResource;
use App\Models\Account as Model;
use Illuminate\Support\Facades\DB;

class Account
{
    public function withdraw(float $value, Model $account): BalanceResource
    {
        $balanceValue = $account->balance - $value;
        $movementHistoryValue = $value * -1;

        return $this->changeBalance($account, $balanceValue, $movementHistoryValue);
    }

    public function deposit(float $value, Model $account): BalanceResource
    {
        $balanceValue = $account->balance + $value;
        $movementHistoryValue = $value;

        return $this->changeBalance($account, $balanceValue, $movementHistoryValue);
    }

    private function changeBalance(Model $account, float $balanceValue, float $movementHistoryValue): BalanceResource
    {
        DB::transaction(
            function () use ($account, $balanceValue, $movementHistoryValue) {
                $account->balance = $balanceValue;
                $account->save();
                $this->saveMovementHistory($account, $movementHistoryValue);
            }
        );

        return new BalanceResource($account);
    }

    private function saveMovementHistory(Model $account, float $value): void
    {
        $account
            ->movementHistories()
            ->create(['value' => $value]);
    }
}
