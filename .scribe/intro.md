# Introduction

Esta é a documentação para utilizar nossos endpoints. É necessário utilizar o header <strong>accept: application/json</strong>

As respostas referentes ao status 404 vão parecer erros por causa do retorno que ocorre com toda a pipeline da exceção,
porém isso não ocorre na produção, somente é exibida a mensagem da chave "message", assim como aparece nos exemplos.

> Base URL

```yaml
http://localhost
```