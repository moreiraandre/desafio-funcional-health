<?php

use Illuminate\Support\Facades\Route;

Route::put('sacar/{conta:number}', 'AccountController@withdraw')->name('withdraw');
Route::put('depositar/{conta:number}', 'AccountController@deposit')->name('deposit');
Route::get('saldo/{conta:number}', 'AccountController@balance')->name('balance');
