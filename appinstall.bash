#!/usr/bin/env bash
echo "---> Configurando..."
echo
cp .env-laradock.example laradock/.env
cp -f xdebug.ini.workspace.example laradock/workspace/xdebug.ini
cd laradock
echo
echo "---> Preparando containers..."
echo
docker-compose build nginx mysql
docker-compose up -d nginx mysql
echo
echo "---> Instalando aplicação..."
echo
docker-compose exec --user=laradock workspace zsh -c "composer install ; cp .env.example .env ; php artisan key:generate ; php artisan storage:link ; php artisan migrate ; php artisan migrate --database=mysql-tests; chmod 777 -R storage bootstrap"
echo
echo "---> Executando testes automatizados..."
echo
docker-compose exec --user=laradock workspace zsh -c "XDEBUG_MODE=coverage bin/phpunit --debug --coverage-text"
echo
echo "---> Verificando a qualidade do código..."
echo
docker-compose exec --user=laradock workspace zsh -c "bin/phpcs"
docker-compose exec --user=laradock workspace zsh -c "bin/phpmd app text phpmd.xml"
docker-compose exec --user=laradock workspace zsh -c "bin/phpmd domain text phpmd.xml"
docker-compose exec --user=laradock workspace zsh -c "bin/phpmd tests/Feature text phpmd.xml"
