<?php

namespace Tests\Feature\Account\GraphQl;

use App\Models\Account;
use Tests\TestCase;

class BalanceTest extends TestCase
{
    public function testSuccess()
    {
        $account = Account::factory()->create();

        $response = $this->graphQL(
            "
            {
              account(id: {$account->getKey()}) {
                id
                number
                balance
              }
            }
        "
        );

        $response->assertJson(
            [
                'data' => [
                    'account' => [
                        'id'      => $account->id,
                        'number'  => $account->number,
                        'balance' => $account->balance,
                    ]
                ]
            ]
        );
    }

    public function testFailureAccountNotFound()
    {
        $accountNotFoundId = 0;

        $response = $this->graphQL(
            "
            {
              account(id: {$accountNotFoundId}) {
                id
                number
                balance
              }
            }
        "
        );

        $response->assertJson(
            [
                'data' => [
                    'account' => null
                ]
            ]
        );
    }
}
