<?php

namespace Tests\Feature\Account\GraphQl;

use App\Models\Account;
use Tests\TestCase;

abstract class AbstractBalance extends TestCase
{
    protected const MUTATION = '';

    public function testSuccess()
    {
        $account = Account::factory()->create();
        $value = 10;

        $response = $this->graphQL(
            "
                mutation {
                    " . static::MUTATION . "(
                        account: {$account->number},
                        value: $value
                    ) {
                        id
                        number
                        balance
                    }
                }
                "
        );

        $account->refresh();
        $response->assertJson(
            [
                'data' => [
                    static::MUTATION => [
                        'id'      => $account->id,
                        'number'  => $account->number,
                        'balance' => $account->balance,
                    ]
                ]
            ]
        );
    }

    public function testFailureRequiredFields()
    {
        $account = Account::factory()->create();

        $response = $this->graphQL(
            "
                mutation {
                    " . static::MUTATION . "(
                        account: {$account->number}
                    ) {
                        id
                        number
                        balance
                    }
                }
                "
        );

        $response->assertJson(
            [
                'errors' => [
                    [
                        'message' => 'Field "' . static::MUTATION
                            . '" argument "value" of type "Float!" is required but not provided.'
                    ]
                ]
            ]
        );
    }

    public function testFailureAccountNotFound()
    {
        $accountNotFoundId = 0;
        $value = 10;

        $response = $this->graphQL(
            "
            mutation {
                " . static::MUTATION . "(
                    account: $accountNotFoundId,
                    value: $value
                ) {
                    id
                    number
                    balance
                }
            }
            "
        );

        $response->assertJson(
            [
                'errors' => [
                    [
                        'extensions' => [
                            'validation' => [
                                'account' => ['The selected account is invalid.']
                            ]
                        ]
                    ]
                ]
            ]
        );
    }
}
