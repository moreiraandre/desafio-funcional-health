<?php

namespace Tests\Feature\Account\GraphQl;

use App\Models\Account;

class DepositTest extends AbstractBalance
{
    protected const MUTATION = 'deposit';

    public function testFailureTypeFields()
    {
        $account = Account::factory()->create();
        $value = 'invalid_type';

        $response = $this->graphQL(
            "
                mutation {
                    " . static::MUTATION . "(
                        account: {$account->number},
                        value: $value
                    ) {
                        id
                        number
                        balance
                    }
                }
                "
        );

        $account->refresh();
        $response->assertJson(
            [
                'errors' => [
                    [
                        'message' => 'Field "deposit" argument "value" requires type Float!, found invalid_type.'
                    ]
                ]
            ]
        );
    }
}
