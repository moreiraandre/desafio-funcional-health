<?php

namespace Tests\Feature\Account\Rest;

use App\Models\Account;

class WithdrawTest extends AbstractBalance
{
    protected const ROUTE = 'withdraw';

    public function testFailureTypeFields()
    {
        $account = Account::factory()->create();
        $data = ['value' => 'invalid_type'];

        $response = $this->putJson(
            route(self::ROUTE, $account->number),
            $data
        );

        $response
            ->assertStatus(422)
            ->assertJson(
                [
                    'message' => 'The given data was invalid.',
                    'errors'  => [
                        'value' => [
                            "The value must be a number.",
                            "The value must be greater than 0.",
                            "The value must be less than or equal {$account->balance}."
                        ],
                    ]
                ]
            );
    }
}
