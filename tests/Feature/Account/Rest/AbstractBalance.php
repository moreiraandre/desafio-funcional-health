<?php

namespace Tests\Feature\Account\Rest;

use App\Models\Account;
use Tests\TestCase;

abstract class AbstractBalance extends TestCase
{
    protected const ROUTE = '';

    public function testSuccess()
    {
        $account = Account::factory()->create();
        $data = ['value' => 10];

        $response = $this->putJson(
            route(static::ROUTE, $account->number),
            $data
        );

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'data' => [
                        'account',
                        'balance',
                        'movement_histories' => [
                            [
                                'value',
                                'created_at',
                            ]
                        ],
                    ]
                ]
            );
    }

    public function testFailureRequiredFields()
    {
        $account = Account::factory()->create();

        $response = $this->putJson(
            route(static::ROUTE, $account->number),
        );

        $response
            ->assertStatus(422)
            ->assertJson(
                [
                    'message' => 'The given data was invalid.',
                    'errors'  => [
                        'value' => ['The value field is required.'],
                    ]
                ]
            );
    }

    public function testFailureAccountNotFound()
    {
        $accountNotFaound = 0;

        $response = $this->putJson(
            route(static::ROUTE, $accountNotFaound),
        );

        $response
            ->assertStatus(404)
            ->assertJson(
                [
                    "message" => "No query results for model [App\Models\Account] 0"
                ]
            );
    }
}
