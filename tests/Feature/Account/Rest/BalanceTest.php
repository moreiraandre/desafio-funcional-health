<?php

namespace Tests\Feature\Account\Rest;

use App\Models\Account;
use Tests\TestCase;

class BalanceTest extends TestCase
{
    private const ROUTE = 'balance';

    public function testSuccess()
    {
        $account = Account::factory()->hasMovementHistories()->create();
        $data = ['value' => 10];

        $response = $this->getJson(
            route(static::ROUTE, $account->number),
            $data
        );

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'data' => [
                        'account',
                        'balance',
                        'movement_histories' => [
                            [
                                'value',
                                'created_at',
                            ]
                        ],
                    ]
                ]
            );
    }

    public function testFailureAccountNotFound()
    {
        $accountNotFaound = 0;

        $response = $this->getJson(
            route(static::ROUTE, $accountNotFaound),
        );

        $response
            ->assertStatus(404)
            ->assertJson(
                [
                    "message" => "No query results for model [App\Models\Account] 0"
                ]
            );
    }
}
