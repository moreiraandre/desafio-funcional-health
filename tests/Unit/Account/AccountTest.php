<?php

namespace Tests\Unit\Account;

use App\Http\Resources\Account\Balance as BalanceResource;
use App\Models\Account as Model;
use Domain\Account\Account as AccountDomain;
use ReflectionClass;
use Tests\TestCase;

class AccountTest extends TestCase
{
    private AccountDomain $accountDomain;

    protected function setUp(): void
    {
        parent::setUp();
        $this->accountDomain = new AccountDomain();
    }

    public function testWithdraw()
    {
        $value = 10.10;
        $account = Model::factory()->create();

        $return = $this->accountDomain->withdraw($value, $account);

        $this->assertInstanceOf(BalanceResource::class, $return);
    }

    public function testDeposit()
    {
        $value = 10.10;
        $account = Model::factory()->create();

        $return = $this->accountDomain->deposit($value, $account);

        $this->assertInstanceOf(BalanceResource::class, $return);
    }

    public function testChangeBalance()
    {
        $balanceValue = 1000.10;
        $movementHistoryValue = 10.10;
        $account = Model::factory()->create();

        $return = $this->invokePrivateMethod('changeBalance', [$account, $balanceValue, $movementHistoryValue]);

        $this->assertInstanceOf(BalanceResource::class, $return);
    }

    public function testSaveMovementHistory()
    {
        $value = 10.10;
        $account = Model::factory()->create();

        $return = $this->invokePrivateMethod('saveMovementHistory', [$account, $value]);

        $this->assertNull($return);
    }

    private function invokePrivateMethod(string $method, array $methodArgs)
    {
        $reflectionClass = new ReflectionClass($this->accountDomain);
        $methodChangeBalance = $reflectionClass->getMethod($method);
        $methodChangeBalance->setAccessible(true);

        return $methodChangeBalance->invokeArgs($this->accountDomain, $methodArgs);
    }
}
