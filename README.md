# Desafio - Funcional Health - PHP
André Moreira <andre.mcx1@gmail.com>
- [Desafio](https://github.com/funcional-health/challenge/blob/master/php.md)

## Clone e instalação da Aplicação
Na execução do `appinstall.bash` você será solicitado para confirmar a instalação dos hooks do git no momento da instalação 
das dependências do **Composer**, responda _sim_ (`y`) para todas.
```sh
git clone --recurse-submodules git@gitlab.com:moreiraandre/desafio-funcional-health.git
cd desafio-funcional-health
./appinstall.bash
```

## Comandos docker
É necessário que você execute os comandos dentro da pasta `laradock`, que está na raiz do projeto.

### Iniciar serviços
```bash
docker-compose up -d nginx mysql
```
> Agora a aplicação estará disponível na URL http://localhost

> Para visitar a documentação da `API Rest` acesse http://localhost/docs

### Listar serviços ativos
```bash
docker-compose ps
```

### Entrar no terminal ZSH do container Workspace
```bash
docker-compose exec --user=laradock workspace zsh
```
> Aqui haverá acesso à ferramenta de CLI como o `Composer` por exemplo.

### Parar serviços
```bash
docker-compose down
```
> É necessário que você esteja fora do terminal do container.

## GraphQl
Faça as requisições no endpoint `/graphql
`
### Consultar saldo e demais dados da conta
Requisição
```graphql
{
  account(id: 1) {
    id
    number
    balance
  }
}
```
Resposta
```json
{
    "data": {
        "account": {
            "id": 1,
            "number": 92589,
            "balance": 19.15
        }
    }
}
```

### Sacar
Requisição
```graphql
mutation {
    withdraw(
        account: 92589,
        value: 100
    ) {
        id
        number
        balance
    }
}
```
Resposta sucesso
```json
{
    "data": {
        "withdraw": {
            "id": 1,
            "number": 92589,
            "balance": 80.85
        }
    }
}
```
Resposta validação valor máximo
```json
{
  "errors": [
    {
      "message": "Validation failed for the field [withdraw].",
      "extensions": {
        "validation": {
          "value": [
            "The value must be less than or equal 98745."
          ]
        },
        "category": "validation"
      },
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "withdraw"
      ]
    }
  ],
  "data": {
    "withdraw": null
  }
}
```

### Depositar
Requisição
```graphql
mutation {
    deposit(
        account: 92589,
        value: 10
    ) {
        id
        number
        balance
    }
}
```
Resposta
```json
{
    "data": {
        "withdraw": {
            "id": 1,
            "number": 92589,
            "balance": 80.85
        }
    }
}
```

## Testes
- Testes de funcionalidades estão na pasta `tests/Feature`
- Testes unitários estão na pasta `tests/Unit`

Para executar os testes utilize o comando
```sh
# Fora do container workspace
docker-compose exec --user=laradock workspace zsh -c "XDEBUG_MODE=coverage bin/phpunit --debug --coverage-text"

# Dentro do container workspace
XDEBUG_MODE=coverage bin/phpunit --debug --coverage-text
```

O relatório de **cobertura de código** já está gerado no diretório `storage/coverage-report`, basta você abrir o arquivo `index.html` no navegador.

Se quiser gerar o relatório novamente, utilize o comando
```sh
# Fora do container workspace
docker-compose exec --user=laradock workspace zsh -c "XDEBUG_MODE=coverage bin/phpunit --coverage-html <diretório da sua escolha>"

# Dentro do container workspace
XDEBUG_MODE=coverage bin/phpunit --coverage-html <diretório da sua escolha>
```

## Regra de Negócio
As classes referentes ao Domínio do Negócio estão na pasta `domain`.

## Recursos utilizados
- PHP 8.0
- Laravel 8
- MySql
- GraphQl
- API Rest

# Comentários
A geração de relatórios como `Cobertura de Código por Testes Automatizados` e `Documentação de API` geralmente não é versionada 
junto ao código, geralmente eles são geradas no processo de _deploy_ e disponibilizados em algum endereço privado, porém para 
garantir que essas estejam acessíveis mesmo que haja algum problema na utilização do _Docker_, por exemplo, resolvi versionar 
estes documentos.

# Extras
Assistam o meu tutorial no YouTube :)

https://www.youtube.com/watch?v=tTIWaOMGB5I&list=PLeYEvoL1bU0Yaqg5s0qDrKXvOVXG6WF91
