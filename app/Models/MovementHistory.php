<?php

namespace App\Models;

class MovementHistory extends Model
{
    protected $fillable = [
        'value',
    ];
}
