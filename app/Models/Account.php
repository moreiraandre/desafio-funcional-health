<?php

namespace App\Models;

class Account extends Model
{
    protected $fillable = [
        'number',
        'balance',
    ];

    public function movementHistories()
    {
        return $this->hasMany(MovementHistory::class);
    }
}
