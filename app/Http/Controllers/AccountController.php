<?php

namespace App\Http\Controllers;

use App\Http\Requests\Account\Withdraw as WithdrawRequest;
use App\Http\Requests\Account\Deposit as DepositRequest;
use App\Http\Resources\Account\Balance as BalanceResource;
use App\Models\Account as Model;
use Domain\Account\Account as Domain;

class AccountController extends Controller
{
    /**
     * Sacar
     *
     * Realiza o saque de um determinado valor que deve ser maior que zero e menor ou igual ao saldo
     * @group Account
     * @urlParam conta integer required O número da conta. Example: 54321
     * @responseFile api-responses/Account/BalanceSuccess.json
     * @responseFile 422 api-responses/Account/WithdrawValidate.json
     * @response 404 {"message": "No query results for model [App\\Models\\Account] 925893"}
     */
    public function withdraw(WithdrawRequest $request, Model $conta, Domain $domain)
    {
        return $domain->withdraw($request->input('value'), $conta);
    }

    /**
     * Depositar
     *
     * Realiza o depósito de um determinado valor
     * @group Account
     * @urlParam conta integer required O número da conta. Example: 54321
     * @responseFile api-responses/Account/BalanceSuccess.json
     * @responseFile 422 api-responses/Account/DepositValidate.json
     * @response 404 {"message": "No query results for model [App\\Models\\Account] 925893"}
     */
    public function deposit(DepositRequest $request, Model $conta, Domain $domain)
    {
        return $domain->deposit($request->input('value'), $conta);
    }

    /**
     * Consultar Saldo
     *
     * Retorna o saldo atual da conta
     * @group Account
     * @urlParam conta integer required O número da conta. Example: 54321
     * @responseFile api-responses/Account/BalanceSuccess.json
     * @response 404 {"message": "No query results for model [App\\Models\\Account] 925893"}
     */
    public function balance(Model $conta)
    {
        return new BalanceResource($conta);
    }
}
