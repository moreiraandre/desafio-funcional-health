<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\JsonResource;

class Balance extends JsonResource
{
    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function toArray($request)
    {
        return [
            'account'            => $this->number,
            'balance'            => $this->balance,
            'movement_histories' => MovementHistories::collection(
                $this
                    ->movementHistories()
                    ->latest()
                    ->limit(3)
                    ->get()
            ),
        ];
    }
}
