<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class Withdraw extends FormRequest
{
    public function rules()
    {
        $balance = $this->conta->balance ?? 0;
        return [
            'value' => ['required', 'numeric', 'gt:0', 'lte:' . $balance],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'value' => [
                'description' => 'Valor a ser sacado.',
                'example'     => 322.55
            ],
        ];
    }
}
