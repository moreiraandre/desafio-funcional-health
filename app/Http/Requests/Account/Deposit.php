<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class Deposit extends FormRequest
{
    public function rules()
    {
        return [
            'value' => ['required', 'numeric', 'gt:0'],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'value' => [
                'description' => 'Valor a ser depositado.',
                'example'     => 428.00
            ],
        ];
    }
}
