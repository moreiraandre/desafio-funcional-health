<?php

namespace App\GraphQL\Validators\Mutation;

use App\Models\Account;
use Nuwave\Lighthouse\Validation\Validator;

class DepositValidator extends Validator
{
    public function rules(): array
    {
        return [
            'account' => ['required', 'exists:accounts,number'],
            'value'   => ['required', 'numeric', 'gt:0'],
        ];
    }
}
