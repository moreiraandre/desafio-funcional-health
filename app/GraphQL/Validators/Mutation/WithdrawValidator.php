<?php

namespace App\GraphQL\Validators\Mutation;

use App\Models\Account;
use Nuwave\Lighthouse\Validation\Validator;

class WithdrawValidator extends Validator
{
    public function rules(): array
    {
        $maxValueWithdrawal = 0;
        if ($this->arg('account')) {
            $accountFound = Account::select('balance')
                ->where('number', $this->arg('account'))
                ->first();

            if ($accountFound) {
                $maxValueWithdrawal = $accountFound->balance;
            }
        }

        return [
            'account' => ['required', 'exists:accounts,number'],
            'value'   => ['required', 'numeric', 'gt:0', 'lte:' . $maxValueWithdrawal],
        ];
    }
}
