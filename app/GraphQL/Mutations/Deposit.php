<?php

namespace App\GraphQL\Mutations;

use App\Models\Account as Model;
use Domain\Account\Account as Domain;

class Deposit
{
    private Domain $domain;

    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __invoke($rootValue, array $args)
    {
        $account = Model::where('number', $args['account'])->first();
        $this->domain->deposit($args['value'], $account);

        return $account;
    }
}
