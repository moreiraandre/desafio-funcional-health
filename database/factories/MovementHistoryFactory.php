<?php

namespace Database\Factories;

use App\Models\MovementHistory;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovementHistoryFactory extends Factory
{
    protected $model = MovementHistory::class;

    public function definition()
    {
        return [
            'value' => $this->faker->randomFloat(2, 50, 1000),
        ];
    }
}
