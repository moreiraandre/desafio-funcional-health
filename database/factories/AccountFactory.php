<?php

namespace Database\Factories;

use App\Models\Account;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountFactory extends Factory
{
    protected $model = Account::class;

    public function definition()
    {
        return [
            'number'  => $this->faker->randomNumber(5, true),
            'balance' => $this->faker->randomFloat(2, 50, 1000),
        ];
    }
}
