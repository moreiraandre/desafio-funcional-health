<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovementHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create('movement_histories', function (Blueprint $table) {
            $table->id();

            $table->foreignId('account_id')->constrained();
            $table->float('value');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('movement_histories');
    }
}
